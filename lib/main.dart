import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Victor App'),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage('https://images.unsplash.com/photo-1580327332925-a10e6cb11baa?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=407&q=80'),
              fit: BoxFit.fill
              )
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                // * Texto de inicio de sesion
                Text(
                  'Inicio de sesión',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.bold, 
                    ),
                ),

                // * Campo de correo eléctronico
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10
                  ),
                  child: TextField(
                    autofocus: true,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.black87
                    ),
                    decoration: InputDecoration(
                      hintText: 'Correo eléctronico',
                      filled: true,
                      fillColor: Colors.white,
                      icon: Icon(Icons.email_outlined)
                    ),
                  ),
                ),

                // * Campo para Contraseña
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10
                  ),
                  child: TextField(
                    autofocus: true,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.black87
                    ),
                    decoration: InputDecoration(
                      hintText: 'Contraseña',
                      filled: true,
                      fillColor: Colors.white,
                      icon: Icon(Icons.password_outlined)
                    ),
                    obscureText: true,
                  ),
                ),

                // * Boton para Ingresar
                Container(
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      backgroundColor: Colors.red,
                    ),
                    onPressed: (){},
                    child: Text(
                        'Ingresar',
                        style: TextStyle(
                            fontSize: 18 
                          ),
                      ),
                  )
                )
              ],
              )
            ),
        ),
      ),
    );
  }
}